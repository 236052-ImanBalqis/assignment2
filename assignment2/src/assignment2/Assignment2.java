package assignment2;

import java.util.Scanner;

public class Assignment2 {

	static double litres, distance, miles;
	public static void main(String[] args) {
		String newline = System.lineSeparator();  
		 Scanner sc= new Scanner (System.in);
		 System.out.println("c=Car ");
		 System.out.println("b=Bike ");
		 System.out.println("Enter type of vehicle: ");
		 char vehicle =sc.next().charAt(0);
		 
		 if(vehicle == 'c') {
			 System.out.println("Enter amount for petrol: ");
			 double p = sc.nextInt();
			 litres = p/115;
			 distance = litres*8;
			 miles = distance*0.621;
			 System.out.printf("Litres  for petrol: %.2f   " +newline , litres );
			 System.out.printf("Distance in km  : %.2f "+newline , distance );
			 System.out.printf("Distance in miles : %.2f "+newline , miles );
			 //return distance;
		 }
		 else if (vehicle=='b') {
			 System.out.println("Enter amount for petrol: ");
			 double p = sc.nextInt();
			 litres = p/115;
			 distance = litres*20;
			 miles = distance*0.621;
			 System.out.printf("Litres  for petrol:  %.2f  " +newline , litres );
			 System.out.printf("Distance in km  : %.2f "+newline , distance );
			 System.out.printf("Distance in miles : %.2f "+newline , miles );
			 //return distance;
		 }
		 else {
				System.out.println("error vehicle");
				
			}
	}

}
